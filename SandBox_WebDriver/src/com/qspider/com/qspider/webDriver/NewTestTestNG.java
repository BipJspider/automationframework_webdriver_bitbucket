package com.qspider.com.qspider.webDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

		

import org.testng.annotations.Test;

import com.qspider.webDriver.ObjectMap;
import com.qspider.webDriver.ObjectRepositoryxml;
import com.qspider.webDriver.Objrepository;
import com.sandbox.libraies.Generic;

public class NewTestTestNG {
	WebDriver driver;
	
	String XLpath="./TestDatas/Config.xlsx";
	String SheetName="Sheet1";
	String BrowserType=Generic.getXLCellValue(XLpath, SheetName, 0, 1);
	String URL=Generic.getXLCellValue(XLpath, SheetName, 1, 1);
	
	@BeforeMethod
	public void precondition()
	{
		/*
		String XLpath="./TestDatas/Config.xlsx";
		String SheetName="Sheet1";
		String BrowserType=Generic.getXLCellValue(XLpath, SheetName, 0, 1);*/
		//WebDriver driver;
		if(BrowserType.equals("GC"))
		{
		
		System.setProperty("webdriver.chrome.driver", "C:\\ChromeIEDriver\\chromedriver.exe");	
		 driver = new ChromeDriver();
		Reporter.log("Lounching Chrome Browser",true);
		}
		else if(BrowserType.equals("IE")) 
		{
		System.setProperty("webdriver.ie.driver", "C:\\ChromeIEDriver1\\IEDriverServer.exe");	
		 driver = new InternetExplorerDriver();
		Reporter.log("Lounching IE Browser",true);
		}
		else
		{
			 driver=new FirefoxDriver();	
			Reporter.log("Lounching FF Browser",true);
		}
		
		//String URL=Generic.getXLCellValue(XLpath, SheetName, 1, 1);
		driver.get(URL);
		Reporter.log("Navigating to URL"+URL, true);
		
		driver.manage().window().maximize();
		/*

		// driver.findElement(By.id("userName")).sendKeys("CBUNTE");
		driver.findElement(By.xpath("//input[@id='userName']")).sendKeys(
				"CBUNTE");
		driver.findElement(By.id("j_password")).sendKeys("111111");
		// driver.findElement(By.id("sign-in-btn")).click();

		WebElement submitbuttonClick = driver.findElement(By.id("sign-in-btn"));

		submitbuttonClick.submit();

		Reporter.log("Open Browser", true);*/

	}
	

	@AfterMethod
	
	public void postcondition()
	{
		driver.quit();
		Reporter.log("Closing Browser", true);
		
	}	
	@Test 
	public void test1() throws Exception
	{
		System.out.println("Going to Execute Test1");
		//driver.get(URL);
		//driver.findElement(By.xpath("//*[@id='signInTitle']")).click();
		//Get current working directory
	    String workingDir=System.getProperty("user.dir");
	    //Get object map file
	    ObjectMap objmap = new ObjectMap (workingDir+"\\objectpropertiesfile\\objectmap.properties");
	      //Get the username element
	    // WebElement username = driver.findElement(objmap.getLocator("Username_field"));
	    // username.sendKeys("test");
	    
	    System.out.println("Going to click our Element");
	     
	    driver.findElement(objmap.getLocator("SingINLink")).click(); // This step is working using ObjectMap class and objectmap.properties
	     
	    //driver.findElement(ObjectRepositoryxml.getLocator("SingINLink")).click();
	    
	     System.out.println("Successfully clicked Element");
	     /*
	     //Get the password element
	     WebElement password = driver.findElement(objmap.getLocator("Password_field"));
	     password.sendKeys("XXXXXX");//password is omitted
	    //Click on the login button
	     WebElement login = driver.findElement(objmap.getLocator("Login_button"));
	     login.click();
	     Thread.sleep(3000); 
	     //Assert the user login by checking the Online user
	     WebElement onlineuser=driver.findElement(objmap.getLocator("online_user"));*/
	     
	}

	@Test
	public void test2() throws Exception
	{
		Objrepository objTest =new Objrepository();
		/*
		String s=  objTest.objRepository("SingINLink");
		System.out.println("s::" + s);
		driver.findElement(By.xpath(s)); */
		
		//driver.findElement(objTest.objRepository("SingINLink")).click();
		driver.findElement(By.xpath(objTest.objRepository("SingINLink"))).click();
		System.out.println("Called from 2n d Method");
						
	}
	
	
}
