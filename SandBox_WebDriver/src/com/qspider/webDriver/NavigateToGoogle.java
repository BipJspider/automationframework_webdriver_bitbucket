package com.qspider.webDriver;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import com.sun.jna.platform.FileUtils;


public class NavigateToGoogle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		       //WebDriver driver=new FirefoxDriver();
				//WebDriver driver = new ChromeDriver();
				WebDriver driver = new InternetExplorerDriver();
				
				
				// Goto login page
				
				//driver.navigate().to("http://m040167.sungardhe.com:8980/FacultyAttendanceTrackingSsb/login/auth");
				driver.get("http://m040167.sungardhe.com:8980/FacultyAttendanceTrackingSsb/login/auth");
				
				// maximize the window
				
				driver.manage().window().maximize();
				
				//driver.findElement(By.id("userName")).sendKeys("CBUNTE");
				driver.findElement(By.xpath("//input[@id='userName']")).sendKeys("CBUNTE");
				driver.findElement(By.id("j_password")).sendKeys("111111");
				//driver.findElement(By.id("sign-in-btn")).click();
				
				WebElement submitbuttonClick=driver.findElement(By.id("sign-in-btn"));
				
				submitbuttonClick.submit();
				
				//Get the tile of the page and print it
				
				String s=driver.getTitle();
				System.out.println(s);
					
		
		WebElement Term=driver.findElement(By.xpath("//*[@id='table-header']/span[1]"));
		
		System.out.println("Name of the Field is: " +Term.getAttribute("name"));
		System.out.println("Class of the Field is: " +Term.getAttribute("class"));
		System.out.println("LOCATION of the Field is: " +Term.getLocation());
		System.out.println("SIZE of the Field is: " +Term.getSize());
		System.out.println("TEXT of the Field is: " +Term.getText());
		System.out.println("Whether Field is displayed: " +Term.isDisplayed());
		
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		driver.manage().timeouts().implicitlyWait(200, TimeUnit.SECONDS);
		
		System.out.println(scrFile.getAbsolutePath());
		
		try {
			org.apache.commons.io.FileUtils.copyFile(scrFile, new File("C:\\temp\\test.png"));
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		driver.close();
		System.exit(0);
		

	}

}
