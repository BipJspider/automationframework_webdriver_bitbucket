package com.qspider.webDriver;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import org.openqa.selenium.By;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;

public class ObjectRepositoryxml {
	
	
	public static By getLocator(String string) {
        try {
            File inputfile = new File(System.getProperty("user.dir")
                    + "\\OR.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder docbuilder = dbFactory.newDocumentBuilder();
            Document doc = docbuilder.parse(inputfile);
            doc.getDocumentElement().normalize();
            /*
            public By getLocator(String ElementName) throws Exception {
   	         //Read value using the logical name as Key
   	         String locator = properties.getProperty(ElementName);
   	         //Split the value which contains locator type and locator value
   	         
   	         
   	         String locatorType = locator.split(":")[0];
   	         String locatorValue = locator.split(":")[1];
   	         */
   	      
            
            if(doc.getDocumentElement().equals("xpath"))
            	return By.xpath(string);
            /*
   	         
   	         //Return a instance of By class based on type of locator
   	           if(locatorType.toLowerCase().equals("id"))
   	                 return By.id(string);
   	           else if(locatorType.toLowerCase().equals("name"))
   	                 return By.name(string);
   	           else if((locatorType.toLowerCase().equals("classname")) || (locatorType.toLowerCase().equals("class")))
   	                 return By.className(string);
   	           else if((locatorType.toLowerCase().equals("tagname")) || (locatorType.toLowerCase().equals("tag")))
   	                 return By.className(string);
   	           else if((locatorType.toLowerCase().equals("linktext")) || (locatorType.toLowerCase().equals("link")))
   	                 return By.linkText(string);
   	           else if(locatorType.toLowerCase().equals("partiallinktext"))
   	                 return By.partialLinkText(string);
   	           else if((locatorType.toLowerCase().equals("cssselector")) || (locatorType.toLowerCase().equals("css")))
   	                 return By.cssSelector(string);
   	           else if(locatorType.toLowerCase().equals("xpath"))
   	                 return By.xpath(string);
   	           else
   	                   throw new Exception("Locator type '" + locatorType + "' not defined!!");
   	         }
            */
            
            /*
            System.out.println("Root element :"
                    + doc.getDocumentElement().getNodeName());
            NodeList nodelist = doc.getElementsByTagName("Object");
            
            
            System.out.println("##################################");
            for (int tmp = 0; tmp < nodelist.getLength(); tmp++) {
                Node nNode = nodelist.item(tmp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    System.out.println("object name : "
                            + eElement.getAttribute("name"));
                    System.out.println("propertytype : "
                            + eElement.getElementsByTagName("propertytype")
                            .item(0).getTextContent());
                    System.out.println("propertyvalue: "
                            + eElement.getElementsByTagName("propertyvalue")
                            .item(0).getTextContent());
                    System.out.println("--------------------------------------");
                }
            }*/
        } 
        catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
        }
		return null;
    }

}


