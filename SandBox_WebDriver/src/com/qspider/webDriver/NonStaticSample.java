package com.qspider.webDriver;

public class NonStaticSample {
	int i=139;
	void test2()
	{
		System.out.print("\nRunning Nonstatic test2() Method\n");
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.print("Program starts..............\n");
		
		//Refer non static member of class
		
		NonStaticSample refval; // Declaration 
		
		refval = new NonStaticSample(); //Initialization 
		// a new instance of calss NonStaticSample is stored in refval		
		
       //NonStaticSample refval = new NonStaticSample(); //Initialization 
		
		System.out.print("Value of i is " +refval.i);
		
		refval.test2(); // Method Invocation
		
		System.out.print("Program Ends..............");		

	}

}
