package com.qspider.webDriver;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class ReadData_Excel {

	public static void main(String[] args) throws InvalidFormatException, IOException {
		
		FileInputStream fis=new FileInputStream("C:/Workspace/book1.xlsx");
		Workbook wb=WorkbookFactory.create(fis);
		Sheet s=wb.getSheet("sheet1");
		Row r=s.getRow(0);
		Cell c=r.getCell(0);
		int rc=s.getLastRowNum();
		
		s.getRow(0).createCell(1).setCellValue("JAVA");		
		FileOutputStream fos=new FileOutputStream("C:/Workspace/book1.xlsx");
		wb.write(fos);		
		
		String v=c.getStringCellValue();
		System.out.print(v);
		System.out.print("\n Total row count is" +rc);

	}

}
