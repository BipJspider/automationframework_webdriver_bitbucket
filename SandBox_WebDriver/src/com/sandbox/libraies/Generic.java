package com.sandbox.libraies;

import java.io.FileInputStream;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class Generic {

	public static int getXLRowCount(String XLpath, String Sheetname) {
		int rowcount;
		try {
			FileInputStream fis = new FileInputStream(XLpath);
			Workbook wb = WorkbookFactory.create(fis);
			Sheet s = wb.getSheet(Sheetname);
			rowcount = s.getLastRowNum();

		} catch (Exception e) {
			rowcount = -1;
		}
		return rowcount;

	}

	public static String getXLCellValue(String XLpath, String SheetName,
			int rownum, int cellnum) {
		String cellValue;
		try {

			FileInputStream fis = new FileInputStream(XLpath);
			Workbook wb = WorkbookFactory.create(fis);
			Sheet s = wb.getSheet(SheetName);
			cellValue = s.getRow(rownum).getCell(cellnum).getStringCellValue();
		} catch (Exception e) {
			cellValue = " ";
		}
		return cellValue;
	}

	public static void explicitwait(int sec) {
		try {
			Thread.sleep(sec * 1000);
		} catch (InterruptedException e) {

		}

	}
}
